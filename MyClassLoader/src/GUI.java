import static java.lang.System.getProperty;
import static javax.swing.JFileChooser.APPROVE_OPTION;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import lab_03.Status;
import lab_03.StatusListener;

public class GUI {

	private List<MyClassLoader> classLoaders = new ArrayList<MyClassLoader>();
	private JTable table;
	private JTextField textField;

	public static void main(String[] args) {
		new GUI();
	}

	public GUI() {
		JFrame frame = new JFrame();
		frame.getContentPane().setLayout(null);
		frame.setSize(new Dimension(595, 338));

		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(getProperty("user.home")));

		JButton btnLoadClass = new JButton("Load classes");
		btnLoadClass.addActionListener(e -> fileChooser(btnLoadClass, fileChooser));
		btnLoadClass.setBounds(12, 13, 141, 25);
		frame.getContentPane().add(btnLoadClass);

		JButton btnUnloadClasses = new JButton("Unload classes");
		btnUnloadClasses.setBounds(12, 276, 141, 25);
		btnUnloadClasses.addActionListener(e -> unloadClass());
		frame.getContentPane().add(btnUnloadClasses);

		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		Object columnNames[] = { "Class", "State", "Result" };
		table = new JTable(new DefaultTableModel(new Object[0][3], columnNames));
		table.setBounds(12, 50, 569, 174);
		JScrollPane tableContainer = new JScrollPane(table);
		tableContainer.setLocation(12, 50);
		tableContainer.setSize(569, 214);
		frame.getContentPane().add(tableContainer);

		JButton btnInvoke = new JButton("Invoke");
		btnInvoke.addActionListener(e -> invokeProcess());

		btnInvoke.setBounds(464, 13, 117, 25);
		frame.getContentPane().add(btnInvoke);

		textField = new JTextField();
		textField.setBounds(311, 16, 141, 19);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		frame.setVisible(true);
	}

	private void unloadClass() {
		
		int row = table.getSelectedRow();
		classLoaders.remove(row);
		((DefaultTableModel) table.getModel()).removeRow(row);
		System.gc();
	}

	private void fileChooser(JButton btnLoadClass, JFileChooser fileChooser) {

		int result = fileChooser.showOpenDialog(btnLoadClass);

		if (result == APPROVE_OPTION) {
			Class<?> classData = null;
			MyClassLoader classLoader = new MyClassLoader();
			try {
				String fullPath = fileChooser.getSelectedFile().getAbsolutePath();
				String className = fullPath.split("src/")[1].replace("/", ".").replace(".class", "");
				
				if (isClassAlreadyLoaded(className)){
					return;
				}
				
				classData = classLoader.loadTheClass(fullPath);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

			if (classData == null) {
				System.out.println("Class not found");
				return;
			}

			DefaultTableModel model = (DefaultTableModel) table.getModel();
			model.addRow(new Object[] { classData.getName(), "0", "" });
			classLoaders.add(classLoader);
		}
	}

	private boolean isClassAlreadyLoaded(String className) {

		return classLoaders.stream()
				.filter(loader -> loader.getMyClass().getName().equals(className))
				.findFirst()
				.isPresent();
	}

	private void invokeProcess() {

		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {

				int selectedRow = table.getSelectedRow();

				StatusListener sl = new StatusListener() {

					@Override
					public void statusChanged(Status status) {

						table.setValueAt(status.getProgress(), selectedRow, 1);
					}
				};

				try {
					MyClassLoader processor = classLoaders.get(selectedRow);
					Object obj = processor.getMyClass().newInstance();
					Method method = processor.getMyClass().getMethod("process", String.class, StatusListener.class);
					String result = (String) method.invoke(obj, textField.getText(), sl);
					table.setValueAt(result, selectedRow, 2);
				} catch (IllegalAccessException | InstantiationException | NoSuchMethodException | SecurityException
						| IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		});

		t.start();

	}
}
