import java.io.FileInputStream;

public class MyClassLoader extends ClassLoader {

	private Class<?> myClass;


	private byte loadClassData(String className)[] {

		FileInputStream fi;

		byte result[];
		try {
			fi = new FileInputStream(className);
			result = new byte[fi.available()];

			fi.read(result);

			fi.close();
			return result;
		} catch (Exception e) {

			e.printStackTrace();
			return null;
		}
	}

	public synchronized Class<?> loadTheClass(String fullPath) throws ClassNotFoundException {

		String className = fullPath.split("src/")[1].replace("/", ".").replace(".class", "");

		Class<?> result;
		byte classData[];

		try {
			result = super.findSystemClass(className);
			return result;
		} catch (Exception e) {
			//IGNORE
		}

		classData = loadClassData(fullPath);

		if (classData == null) {
			throw new ClassNotFoundException(className);
		}

		result = defineClass(null, classData, 0, classData.length);

		if (result == null) {
			throw new ClassNotFoundException(className);
		}

		resolveClass(result);

		myClass = result;

		return result;
	}
	
	public Class<?> getMyClass(){
		return myClass;
	}
}
