package lab_03;


public interface Processor {

	public String getInfo();
	public String process(String input, StatusListener statusListener);
}
