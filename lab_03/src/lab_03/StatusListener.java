package lab_03;

public interface StatusListener {

	public void statusChanged(Status status);
}
