package lab_03;

import static java.lang.Thread.sleep;

public class StringUpperCaser implements Processor{

	Status status = new Status();
	
	@Override
	public String getInfo() {
		return "Converts all characters in input String to upper case";
	}

	@Override
	public String process(String input, StatusListener statusListener) {

		while (status.getProgress() < 100) {
			status.setProgress(status.getProgress() + 10);
			statusListener.statusChanged(status);
			try {
				sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		return input.toUpperCase();
	}
}
