package lab_03;

import static java.lang.Thread.sleep;

public class StringNumbersRemover implements Processor{

	Status status = new Status();
	
	@Override
	public String getInfo() {
		return "Removes all numeric characters from given input";
	}

	@Override
	public String process(String input, StatusListener statusListener) {

		while (status.getProgress() < 100) {
			status.setProgress(status.getProgress() + 10);
			statusListener.statusChanged(status);
			try {
				sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		return input.replaceAll("\\d+", "");
	}
}
