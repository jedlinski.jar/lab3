package lab_03;

public class Status {

	private String info;
	private int progress;
	
	public String getInfo() {
		return info;
	}
	
	public int getProgress() {
		return progress;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}
}
